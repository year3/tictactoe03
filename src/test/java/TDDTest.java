/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.ntc.lab03.OXProgram;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author L4ZY
 */
public class TDDTest {

    public TDDTest() {
    }

    @Test

    public void testCheckVerticalPlayerOCol1Win() {
        char table[][] = {{'O', '-', '-'},
        {'O', '-', '-'},
        {'O', '-', '-'}};
        char currentPlayer = 'O';
        int col = 1;
        assertEquals(true, OXProgram.checkVertical(table, currentPlayer, col));
    }

    @Test
    public void testCheckHorizontalPlayerORow1Win() {
        char table[][] = {{'O', 'O', 'O'},
        {'-', '-', '-'},
        {'-', '-', '-'}};
        char currentPlayer = 'O';
        int row = 1;
        assertEquals(true, OXProgram.checkHorizontal(table, currentPlayer, row));
    }
    @Test
     public void testCheckHorizontalPlayerORow2Win() {
         char table[][] = {{'O', 'O', 'O'},
                                  {'-', '-', '-'},
                                  {'-', '-', '-'}};
         char currentPlayer = 'O';
         int row=2;
         assertEquals(false,OXProgram.checkHorizontal(table,currentPlayer,row));
     }
     @Test
     public void testCheckHorizontalPlayerORow3Win() {
         char table[][] = {{'O', 'O', 'O'},
                                  {'-', '-', '-'},
                                  {'-', '-', '-'}};
         char currentPlayer = 'O';
         int row=1;
         assertEquals(true,OXProgram.checkHorizontal(table,currentPlayer,row));
     }

    

}
